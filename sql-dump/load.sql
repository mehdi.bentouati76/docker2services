use RIL;

CREATE TABLE IF NOT EXISTS `Test` (
	`id` int not null auto_increment,
	`nom` varchar(20),
	primary key (id)
);

INSERT INTO Test(nom) VALUES ('hello'),('world');
